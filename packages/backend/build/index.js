"use strict";
exports.__esModule = true;
var express = require("express");
var path_1 = require("path");
var app = express();
app.get('/', function (_req, res) { return res.json({ foo: 'bar' }); });
var port = process.env.PORT || 5000;
var isProd = process.env.NODE_ENV === 'production';
if (isProd) {
    var buildPath = path_1["default"].resolve(__dirname, '../../frontend/build');
    var indexHtml_1 = path_1["default"].join(buildPath, 'index.html');
    app.use(express.static(buildPath));
    app.get('*', function (_req, res) { return res.sendFile(indexHtml_1); });
}
app.listen(port);
