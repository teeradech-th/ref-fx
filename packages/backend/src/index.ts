import * as express from 'express';

import path from 'path';

const app: express.Application = express();
app.get('/', (_req : express.Request, res : express.Response) => res.json({ foo: new Date() }));

const port = process.env.PORT || 5000;
const isProduction: boolean = process.env.NODE_ENV === 'production';
if (isProduction) {
  const buildPath = path.resolve(__dirname, '../../frontend/build');
  const indexHtml = path.join(buildPath, 'index.html');

  app.use(express.static(buildPath));
  app.get('*', (_req: express.Request, res: express.Response) => res.sendFile(indexHtml));
}

app.listen(port);
